import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-vente',
  templateUrl: './vente.page.html',
  styleUrls: ['./vente.page.scss'],
})
export class VentePage implements OnInit {

  constructor(private publc:PublicService) { }

  result : any ;

  ngOnInit() {
    this.onLoadVente();
  }

  onLoadVente() {
    this.publc.getVente()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
    },err=>{
      console.log(err); 
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getVente()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
     event.target.complete();

    },err=>{
      console.log(err); 
    });
  }

 deleteVente(id){
   this.publc.deleteVente(id).then(()=>{
     this.onLoadVente();
   })
 }

 updateVente(id){
   this.publc.updateVente(id).then(()=>{
     this.onLoadVente();
   })
 }

}
 

 /*
  doInfinite(IonInfiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() =>{
      for (let i = 0; i < 30; i++) {
        this.result.push(this.result.length);
      }

      console.log('Async operation has ended');
      IonInfiniteScroll.complete();
    }, 500);
  }
  */