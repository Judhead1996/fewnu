import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
//import * as firebase from 'firebase';




@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit{
  
 
 
 
  constructor( public  authService: AuthenticateService ,private route:Router, private alertController:AlertController) {}
 
  ngOnInit(){
  }
 
  async onRegister(value) {
  if(value.password == value.cpassword){
    this.authService.createUser({
      phone:value.phone,
      password:value.password,
      nom_complet:value.nom_complet,
    })
    .then(data =>{
      this.route.navigateByUrl('/home');
      console.log(data);
   }, err => {
      console.log(err);
   });
   console.log(value);
  }
    else{
      const alert = await this.alertController.create({
        header: 'Erreur',
        message: "Les Mots des passes ne sont pas les memes",
        buttons: ['ok'],
      });
      await alert.present();
    }
  }
   
}

  
