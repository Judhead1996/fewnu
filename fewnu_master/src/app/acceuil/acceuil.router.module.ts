import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcceuilPage } from './acceuil.page';

const routes: Routes = [
  {
    path: '',
    component: AcceuilPage,
  /*  children: [
        {
          path: 'vente',
          children: [
            {
              path: '',
              loadChildren: '../vente/vente.module#VentePageModule'
            }
          ]
        },
        {
          path: 'depense',
          children: [
            {
              path: '',
              loadChildren: '../depense/depense.module#DepensePageModule'
            }
          ]
        },
        {
          path: 'pret',
          children: [
            {
              path: '',
              loadChildren: '../pret/pret.module#PretPageModule'
            }
          ]
        },
        {
          path: '',
          redirectTo: './acceuil/vente',
          pathMatch: 'full'
        }
      ]
    },
    {
      path: '',
      redirectTo: './acceuil/vente',
      pathMatch: 'full'*/
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AcceuilPageRoutingModule {}
