import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.page.html',
  styleUrls: ['./acceuil.page.scss'],
})
export class AcceuilPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public logo = {
    logo : "assets/images/mc2.png",

  }

}
