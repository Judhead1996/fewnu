import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'inscription', loadChildren: './inscription/inscription.module#InscriptionPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'ajoutvente', loadChildren: './ajoutvente/ajoutvente.module#AjoutventePageModule' },
  { path: 'ajoutdepense', loadChildren: './ajoutdepense/ajoutdepense.module#AjoutdepensePageModule' },
  { path: 'ajoutpret', loadChildren: './ajoutpret/ajoutpret.module#AjoutpretPageModule' },
  { path: 'calculatrice', loadChildren: './calculatrice/calculatrice.module#CalculatricePageModule' },
  { path: 'journalier', loadChildren: './journalier/journalier.module#JournalierPageModule' },
  { path: 'mensuel', loadChildren: './mensuel/mensuel.module#MensuelPageModule' },
  { path: 'acceuil', loadChildren: './acceuil/acceuil.module#AcceuilPageModule' },
  { path: 'gestionstckok', loadChildren: './gestionstckok/gestionstckok.module#GestionstckokPageModule' },
  { path: 'entrees', loadChildren: './entrees/entrees.module#EntreesPageModule' },
  { path: 'sorties', loadChildren: './sorties/sorties.module#SortiesPageModule' },
  { path: 'ajoutentre', loadChildren: './ajoutentre/ajoutentre.module#AjoutentrePageModule' },
  { path: 'ajoutsorties', loadChildren: './ajoutsorties/ajoutsorties.module#AjoutsortiesPageModule' },
  { path: 'nouscontacter', loadChildren: './nouscontacter/nouscontacter.module#NouscontacterPageModule' },
  { path: 'aprospos', loadChildren: './aprospos/aprospos.module#AprosposPageModule' },
  { path: 'ajoutproduit', loadChildren: './ajoutproduit/ajoutproduit.module#AjoutproduitPageModule' },
  { path: 'hist-sorti', loadChildren: './hist-sorti/hist-sorti.module#HistSortiPageModule' },
  { path: 'hist-entrer', loadChildren: './hist-entrer/hist-entrer.module#HistEntrerPageModule' },
  { path: 'modifier-produit/:id', loadChildren: './modifier-produit/modifier-produit.module#ModifierProduitPageModule' },
  

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
