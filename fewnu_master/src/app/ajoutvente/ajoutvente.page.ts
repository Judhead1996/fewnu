import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-ajoutvente',
  templateUrl: './ajoutvente.page.html',
  styleUrls: ['./ajoutvente.page.scss'],
})
export class AjoutventePage implements OnInit {

  constructor( public  publicService: PublicService, private router:Router ) { }

  ngOnInit() {
  }

  
 onAjout(vente) {
  var id = localStorage.getItem('userId');
     this.publicService.ajoutvente({
       Designation: vente.Designation,
       Prix:vente.Prix,
       id_userv:id,
     })
     .then(data =>{
       console.log(data);
       if(data){
        this.router.navigateByUrl('/menu/acceuil/menu/acceuil/vente');
        console.log('ajout réussi')
      }else{
        this.router.navigateByUrl('ajoutvente');
        console.log('échec') 
      }
    }, err => {
       console.log(err);
    });
    console.log(vente);
   
 }

}
