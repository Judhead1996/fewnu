import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistSortiPage } from './hist-sorti.page';

describe('HistSortiPage', () => {
  let component: HistSortiPage;
  let fixture: ComponentFixture<HistSortiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistSortiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistSortiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
