import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GestionstckokPage } from './gestionstckok.page';

const routes: Routes = [
  {
    path: 'menu/gestionstckok',
    component: GestionstckokPage,
    children: [
      {
        path: 'entrees',
        children: [
          {
            path: '',
            loadChildren: '../entrees/entrees.module#EntreesPageModule'
          }
        ]
              },
      {
        path: 'sorties',
        children: [
          {
            path: '',
            loadChildren: '../sorties/sorties.module#SortiesPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'menu/gestionstckok/entrees',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu/gestionstckok/entrees',
    pathMatch: 'full'
}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GestionstckokPage]
})
export class GestionstckokPageModule {}
