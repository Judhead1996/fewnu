import { Injectable } from '@angular/core';
import {rootUrl} from "../../app/apiurls/serverurls.js";
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private http:HTTP) { }

  getVente() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/vente/"+id+"/", {}, {});  
  }

  getDepense() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/depense/"+id+"/", {}, {});  
  }

  getPret() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/pret/"+id+"/", {}, {});  
  }

  getEntree(){
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/Entree/"+id+"/", {}, {});
  }

  getSortie(){
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/Sortie/"+id+"/", {}, {});
  }
///////////////////////////////////////////////////////////////////////////////

  deleteVente(id){
    return this.http.delete(rootUrl+`/api/ventedelete/${id}/`, {},{})
  }

  updateVente(id: string){
    return this.http.patch(rootUrl+`/api/venteupdate/${id}/`, {},{})
  }

  getCustomer(id: string){
    return this.http.get(rootUrl+`/api/vente/${id}/`, {}, {}); 
  }

///////////////////////////////////////////////////////////////////////////////
  ajoutvente(vente){
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl+"/api/vente/"+id+"/", vente, {});
  }

  ajoutdepense(depense){
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl+"/api/depense/"+id+"/", depense, {});
  }

}
