import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-modifier-produit',
  templateUrl: './modifier-produit.page.html',
  styleUrls: ['./modifier-produit.page.scss'],
})
export class ModifierProduitPage implements OnInit {

  result = [] ;
  id : number;
  data: object = {};

  constructor(public  publc:PublicService, private router:Router, private route: ActivatedRoute ) { }

  ngOnInit() {
  /* this.route.params.subscribe(params => {
     this.id = +params['id'];
   })
   this.publc.getVente()
   .then(data=>{
    
    this.result = JSON.parse(data.data);
      for(var i = 0; i < this.result.lenght; i++){
        if(parseInt(this.result[i].id) === this.id) {
          this.data = this.result[i];
          break;
        }
      }
   }
   ); */
  }

  onModif(product){
    
  }

  updateVente(id){
    this.publc.updateVente(id).then(()=>{
     // this.onLoadVente();
     this.router.navigateByUrl('/menu/acceuil/menu/acceuil/vente');
    })
  }

   deleteVente(id){
    this.publc.deleteVente(id).then(()=>{
     // this.onLoadVente();
     this.router.navigateByUrl('/menu/acceuil/menu/acceuil/vente');
    })
  }


}
